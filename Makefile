book.name := parkinson,cyril_northcote__the_law_and_the_profits
src := src

book.id := $(shell echo "$(book.name)" | sha1sum | head -c 7)
out := _out
book := $(out)/$(book.id)
book.epub := $(out)/$(book.name).epub
book.mobi := $(out)/$(book.name).mobi

all: res

svg.src := $(wildcard $(src)/drawings/*.jpg)
svg.dest := $(patsubst $(src)/%.jpg, $(book)/%.svg, $(svg.src))
res = $(svg.dest)

$(book)/%.svg: $(src)/%.jpg
	$(mkdir)
	convert $< $(book)/$*.pgm
	potrace -s -O 10 -u 1 -k 0.5 -W 8 -o $@.tmp $(book)/$*.pgm
	echo '<?xml version="1.0" encoding="UTF-8"?>' > $@
	sed 1,3d < $@.tmp >> $@
	rm $(book)/$*.pgm $@.tmp

static.src := $(wildcard $(src)/*.xhtml $(src)/*.css $(src)/*jpg)
static.dest := $(patsubst $(src)/%, $(book)/%, $(static.src))

$(static.dest): $(book)/%: $(src)/%; $(copy)
res += $(static.dest)

meta.req := $(res) $(src)/meta.yaml
$(book)/%.opf: %.erb $(meta.req); $(erb)
$(book)/%.xhtml: %.erb $(meta.req); $(erb)
res += $(addprefix $(book)/, content.opf nav.xhtml colophon.xhtml)

$(book)/META-INF/container.xml: META-INF/container.xml; $(copy)
$(book)/cover.xhtml: cover.xhtml; $(copy)
res += $(book)/META-INF/container.xml $(book)/cover.xhtml

$(book)/cover.png: $(src)/cover.svg
	inkscape $< -w 1600 -h 2560 -o $@
res += $(book)/cover.png

$(book.epub): $(res)
	zip -X0 -j $@.tmp mimetype
	cd $(book) && zip -r "$(CURDIR)/$@.tmp" *
	epub-hyphen -i table $@.tmp -o $@
	@rm $@.tmp

res: $(res)
mobi: $(book.mobi)
%.mobi: %.epub; kindlegen $<

epub: $(book.epub)
mobi: $(book.mobi)
lint: $(book.epub); epubcheck $<



mkdir = @mkdir -p $(dir $@)

define copy =
$(mkdir)
cp $< $@
endef

define erb =
$(mkdir)
src=$(src) out=$(book) erb $< > $@
endef

.DELETE_ON_ERROR:
